import React, { useState } from 'react';
import classes from './VatTax.module.css';

import { Divider } from '@mui/material';
import BasicSelect from './FlashDeal';

export const VatTax = () => {
  return (
    <>
      <div className={classes.shipping}>
        <div className={classes.title}>Vat & TAX Quantity Warning</div>
        <Divider />
        <div className={classes.body}>
          <div className={classes.body__wrapper}>
            <span className={classes.body__content}>
              <span>
                <label>Tax:</label>
              </span>
              <span className={classes.items}>
                <span className={classes.input}>
                  <input
                    style={{ outline: 'none' }}
                    type="number"
                    placeholder="1"
                  />
                </span>
                <span className={classes.input}>
                  <BasicSelect menuItem={['Flat', 'Percentage']} />
                </span>
              </span>
            </span>
            <span className={classes.body__content}>
              <span>
                <label>Vat:</label>
              </span>
              <span className={classes.items}>
                <span className={classes.input}>
                  <input
                    style={{ outline: 'none' }}
                    type="number"
                    placeholder="1"
                  />
                </span>
                <span className={classes.input}>
                  <BasicSelect menuItem={['Flat', 'Percentage']} />
                </span>
              </span>
            </span>
          </div>
        </div>
      </div>
    </>
  );
};
