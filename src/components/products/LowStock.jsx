import React, { useState } from 'react';
import classes from './LowStock.module.css';

import { Divider, Switch } from '@mui/material';

export const LowStock = () => {
  const [catOptions, setCatOptions] = useState(false);
  const [brandOptions, setBrandOptions] = useState(false);
  return (
    <>
      <div className={classes.shipping}>
        <div className={classes.title}>Low Stock Quantity Warning</div>
        <Divider />
        <div className={classes.body}>
          <div className={classes.body__wrapper}>
            <span className={classes.body__content}>
              <span>
                <label>Quantity</label>
              </span>
              <span className={classes.input}>
                <input
                  style={{ outline: 'none' }}
                  type="number"
                  placeholder="1"
                />
              </span>
            </span>
          </div>
        </div>
      </div>
    </>
  );
};
