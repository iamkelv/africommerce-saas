import React, { useState } from 'react';
import classes from './ProductInformation.module.css';
import { RiArrowDownSLine } from 'react-icons/ri';
import { Divider, Switch } from '@mui/material';

export const ProductInformation = () => {
  const [catOptions, setCatOptions] = useState(false);
  const [brandOptions, setBrandOptions] = useState(false);
  return (
    <>
      <div className={classes.product_info}>
        <div className={classes.title}>Product Information</div>
        <Divider />
        <div className={classes.form__group}>
          <span className={classes.form__label}>
            <label htmlFor="productName">
              Product Name <span>*</span>
            </label>
          </span>
          <span className={classes.form__input}>
            <input type="text" id="productName" placeholder="Product Name" />
          </span>
        </div>
        <div className={classes.form__group}>
          <span className={classes.form__label}>
            <label htmlFor="productName">
              Category <span>*</span>
            </label>
          </span>
          <span className={classes.form__input}>
            <label
              className={classes.select_option}
              onClick={() => setCatOptions((prev) => !prev)}
            >
              Women Clothing & Fashion <RiArrowDownSLine />
            </label>
            <span
              className={
                catOptions
                  ? `${classes.options_drop} `
                  : `${classes.options_drop} ${classes.hideOption}`
              }
            >
              <input type="text" />
              <span className={classes.default}>Women Clothing & Fashion </span>
              <span>
                {[
                  'Select Brand',
                  'Ford',
                  'Chevrolet',
                  'Audi',
                  'Hyundai',
                  'Innoson',
                ].map((brand) => (
                  <span className={classes.option} key={brand}>
                    {brand}
                  </span>
                ))}
              </span>
            </span>
          </span>
        </div>
        <div className={classes.form__group}>
          <span className={classes.form__label}>
            <label htmlFor="productName">
              Brand <span>*</span>
            </label>
          </span>
          <span className={classes.form__input}>
            <label
              className={classes.select_option}
              onClick={() => setBrandOptions((prev) => !prev)}
            >
              Select Brand <RiArrowDownSLine />
            </label>
            <span
              className={
                brandOptions
                  ? `${classes.options_drop} `
                  : `${classes.options_drop} ${classes.hideOption}`
              }
            >
              <input type="text" />
              <span className={classes.default}>Women Clothing & Fashion </span>
              <span>
                {[
                  'Select Brand',
                  'Ford',
                  'Chevrolet',
                  'Audi',
                  'Hyundai',
                  'Innoson',
                ].map((brand) => (
                  <span className={classes.option} key={brand}>
                    {brand}
                  </span>
                ))}
              </span>
            </span>
          </span>
        </div>
        <div className={classes.form__group}>
          <span className={classes.form__label}>
            <label htmlFor="unit">
              Unit <span>*</span>
            </label>
          </span>
          <span className={classes.form__input}>
            <input type="text" id="unit" placeholder="Unit (e.g. KG, Pc etc)" />
          </span>
        </div>
        <div className={classes.form__group}>
          <span className={classes.form__label}>
            <label htmlFor="weight">
              Weight (In Kg) <span>*</span>
            </label>
          </span>
          <span className={classes.form__input}>
            <input type="text" id="weight" placeholder="0.00" />
          </span>
        </div>
        <div className={classes.form__group}>
          <span className={classes.form__label}>
            <label htmlFor="quantity">
              Minimum Purchase Qty <span>*</span>
            </label>
          </span>
          <span className={classes.form__input}>
            <input type="text" id="quantity" placeholder="1" />
          </span>
        </div>
        <div className={classes.form__group}>
          <span className={classes.form__label}>
            <label htmlFor="tags">
              Tags <span>*</span>
            </label>
          </span>
          <span className={classes.form__input}>
            <input
              type="text"
              id="tag"
              placeholder="Type and hit enter to add a tag"
            />
          </span>
        </div>
        <div className={classes.form__group}>
          <span className={classes.form__label}>
            <label htmlFor="barcode">
              Barcode <span>*</span>
            </label>
          </span>
          <span className={classes.form__input}>
            <input type="text" id="barcode" placeholder="Barcode" />
          </span>
        </div>
        <div className={classes.form__group}>
          <span className={classes.form__label}>
            <label htmlFor="refundable">
              Refundable <span>*</span>
            </label>
          </span>
          <span className={classes.form__input}>
            <span className={classes.switch}>
              <Switch />
            </span>
          </span>
        </div>
      </div>
    </>
  );
};
