import React, { useRef, useState } from 'react';
import classes from './ProductVidoes.module.css';
import { RiArrowDownSLine } from 'react-icons/ri';
import { Divider, Switch } from '@mui/material';

export const ProductVidoes = () => {
  const [catOptions, setCatOptions] = useState(false);

  return (
    <div>
      <div className={classes.product_videos}>
        <div className={classes.title}>Product Videos</div>
        <Divider />
        <div className={classes.form__group}>
          <span className={classes.form__label}>
            <label htmlFor="productName">Video Provider</label>
          </span>
          <span className={classes.form__input}>
            <label
              className={classes.select_option}
              onClick={() => setCatOptions((prev) => !prev)}
            >
              Youtube <RiArrowDownSLine />
            </label>
            <span
              className={
                catOptions
                  ? `${classes.options_drop} `
                  : `${classes.options_drop} ${classes.hideOption}`
              }
            >
              <span className={classes.default}>Youtube </span>
              <span>
                {['Dailymotion', 'Vimoe'].map((brand) => (
                  <span className={classes.option} key={brand}>
                    {brand}
                  </span>
                ))}
              </span>
            </span>
          </span>
        </div>
        <div className={classes.form__group}>
          <span className={classes.form__label}>
            <label htmlFor="videoLink">Video Link</label>
          </span>
          <span className={classes.form__input}>
            <input type="text" id="videoLink" placeholder="Video Link" />
          </span>
        </div>
      </div>
    </div>
  );
};
