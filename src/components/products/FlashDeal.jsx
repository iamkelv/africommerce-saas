import React, { useState } from 'react';
import classes from './FlashDeal.module.css';
import { Divider } from '@mui/material';
import Box from '@mui/material/Box';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

export default function BasicSelect({ menuItem }) {
  const [flash, setFlash] = useState('');

  const handleChange = (event) => {
    setFlash(event.target.value);
  };

  return (
    <Box sx={{ minWidth: 120, margin: '1rem 0' }}>
      <FormControl fullWidth>
        <Select
          labelId="simple-select-label"
          id="simple-select"
          value={flash}
          onChange={handleChange}
        >
          {menuItem.map((item) => (
            <MenuItem value={item}>{item}</MenuItem>
          ))}
        </Select>
      </FormControl>
    </Box>
  );
}
export const FlashDeal = () => {
  return (
    <>
      <div className={classes.shipping}>
        <div className={classes.title}>Flash Deal</div>
        <Divider />
        <div className={classes.body}>
          <div className={classes.body__wrapper}>
            <span className={classes.body__content}>
              <span className={classes.content__name}>Add To Flash</span>
              <span className={classes.input}>
                <BasicSelect
                  menuItem={[
                    'Winter Sell',
                    'Flash Sell',
                    'Electronics',
                    'Flash Deal',
                  ]}
                />
              </span>
            </span>
            <span className={classes.body__content}>
              <span className={classes.content__name}>Discount</span>
              <span className={classes.input__discount}>
                <input type="number" placeholder="0" />
              </span>
            </span>
            <span className={classes.body__content}>
              <span className={classes.content__name}>Discount Type</span>
              <span className={classes.input}>
                <BasicSelect menuItem={['Flat', 'Percentage']} />
              </span>
            </span>
          </div>
        </div>
      </div>
    </>
  );
};
