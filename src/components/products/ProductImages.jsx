import React, { useRef, useState } from 'react';
import classes from './ProductImages.module.css';
import { RiArrowDownSLine } from 'react-icons/ri';
import { Divider, Switch } from '@mui/material';

export const ProductImages = () => {
  const [catOptions, setCatOptions] = useState(false);
  const [brandOptions, setBrandOptions] = useState(false);
  const hiddenFileInput = useRef(null);

  const handleClick = (event) => {
    hiddenFileInput.current.click();
  };
  const handleChange = (event) => {
    const fileUploaded = event.target.files[0];
  };
  return (
    <div>
      <div className={classes.product_images}>
        <div className={classes.title}>Product Images</div>
        <Divider />
        <div className={classes.form__group}>
          <span className={classes.form__label}>
            <label htmlFor="upload">
              <span>Gallery Images</span>
              <span className={classes.sub}>(600x600)</span>
            </label>
          </span>
          <span className={classes.form__input}>
            <input
              type="file"
              id="upload"
              accept=".jpg, .jpeg, .png"
              multiple
            />
            <span style={{ fontSize: '14px', marginTop: '0px' }}></span>
          </span>
        </div>
        <div className={classes.form__group}>
          <span className={classes.form__label}>
            <label htmlFor="upload">
              <span>Thumbnail Image</span>
              <span className={classes.sub}>(300x300)</span>
            </label>
          </span>
          <span className={classes.form__input}>
            <input
              type="file"
              id="upload"
              accept=".jpg, .jpeg, .png"
              multiple
            />
            <span style={{ fontSize: '14px', marginTop: '0px' }}></span>
          </span>
        </div>
      </div>
    </div>
  );
};
