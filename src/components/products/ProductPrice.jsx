import React, { useState } from 'react';
import classes from './ProductPrice.module.css';
import { Divider } from '@mui/material';

export const ProductPrice = () => {
  const [catOptions, setCatOptions] = useState(false);
  const [brandOptions, setBrandOptions] = useState(false);
  return (
    <>
      <div className={classes.product_info}>
        <div className={classes.title}>Product price + stock</div>
        <Divider />
        <div className={classes.form__group}>
          <span className={classes.form__label}>
            <label htmlFor="unit">
              Unit price <span>*</span>
            </label>
          </span>
          <span className={classes.form__input}>
            <input type="text" id="productName" placeholder="0" />
          </span>
        </div>
        <div className={classes.form__group}>
          <span className={classes.form__label}>
            <label htmlFor="unit">Discount Date Range</label>
          </span>
          <span className={classes.form__input}>
            <input type="date" id="productName" placeholder="0" />
          </span>
        </div>
        <div className={classes.form__group}>
          <span className={classes.form__label}>
            <label htmlFor="sellpoint">Set Point</label>
          </span>
          <span className={classes.form__input}>
            <input type="number" id="sellpoint" placeholder="0" />
          </span>
        </div>
        <div className={classes.form__group}>
          <span className={classes.form__label}>
            <label htmlFor="quanity">
              Quantity <span>*</span>
            </label>
          </span>
          <span className={classes.form__input}>
            <input type="number" id="sellpoint" placeholder="0" />
          </span>
        </div>
        <div className={classes.form__group}>
          <span className={classes.form__label}>
            <label htmlFor="sku">SKU</label>
          </span>
          <span className={classes.form__input}>
            <input type="number" id="sku" placeholder="0" />
          </span>
        </div>
        <div className={classes.form__group}>
          <span className={classes.form__label}>
            <label htmlFor="sku">SKU</label>
          </span>
          <span className={classes.form__input}>
            <input type="text" id="sku" placeholder="0" />
          </span>
        </div>
        <div className={classes.form__group}>
          <span className={classes.form__label}>
            <label htmlFor="discount">External link</label>
          </span>
          <span className={classes.form__input}>
            <input type="url" id="link" placeholder="External link" />
          </span>
        </div>
      </div>
    </>
  );
};
