import classes from './ShippingTime.module.css';
import { Divider } from '@mui/material';

export const ShippingTime = () => {
  return (
    <>
      <div className={classes.shipping}>
        <div className={classes.title}>Estimate Shipping Time</div>
        <Divider />
        <div className={classes.body}>
          <div className={classes.body__wrapper}>
            <span className={classes.body__content}>
              <span className={classes.content__name}>Shipping Days</span>
              <span className={classes.input__days}>
                <span className={classes.days__value}>
                  <input type="text" placeholder="0" />
                </span>

                <span className={classes.days__title}>Days</span>
              </span>
            </span>
          </div>
        </div>
      </div>
    </>
  );
};
