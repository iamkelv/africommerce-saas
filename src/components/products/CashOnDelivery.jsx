import React, { useState } from 'react';
import classes from './CashOnDelivery.module.css';
import { Divider, Switch } from '@mui/material';

export const CashOnDelivery = () => {
  const [catOptions, setCatOptions] = useState(false);
  const [brandOptions, setBrandOptions] = useState(false);
  return (
    <>
      <div className={classes.shipping}>
        <div className={classes.title}>Cash On Delivery</div>
        <Divider />
        <div className={classes.body}>
          <div className={classes.body__wrapper}>
            <span className={classes.body__content}>
              <span className={classes.content__name}>Status</span>
              <span className={classes.switch}>
                <Switch />
              </span>
            </span>
          </div>
        </div>
      </div>
    </>
  );
};
