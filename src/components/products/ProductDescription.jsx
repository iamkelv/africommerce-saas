import React, { useState } from 'react';
import classes from './ProductDescription.module.css';
import { Divider } from '@mui/material';

export const ProductDescription = () => {
  const [catOptions, setCatOptions] = useState(false);
  const [brandOptions, setBrandOptions] = useState(false);
  return (
    <>
      <div className={classes.product_info}>
        <div className={classes.title}>Product Description</div>
        <Divider />
        <div
          className={classes.form__group}
          style={{ alignItems: 'flex-start' }}
        >
          <span className={classes.form__label}>
            <label htmlFor="unit">Description</label>
          </span>
          <span className={classes.form__input}>
            <textarea
              style={{ width: '90%', outline: 'none', padding: '1rem' }}
              type="text"
              id="description"
              rows={20}
            />
          </span>
        </div>
      </div>
    </>
  );
};
