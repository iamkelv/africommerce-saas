import React, { useState } from 'react';
import classes from './Shipping.module.css';
import { RiArrowDownSLine } from 'react-icons/ri';
import { Divider, Switch } from '@mui/material';

export const Shipping = () => {
  const [catOptions, setCatOptions] = useState(false);
  const [brandOptions, setBrandOptions] = useState(false);
  return (
    <>
      <div className={classes.shipping}>
        <div className={classes.title}>Shipping Configuration</div>
        <Divider />
        <div className={classes.body}>
          <div className={classes.body__wrapper}>
            <span className={classes.body__content}>
              <span className={classes.content__name}>Free Shipping</span>
              <span className={classes.switch}>
                <Switch />
              </span>
            </span>
            <span className={classes.body__content}>
              <span className={classes.content__name}>Flat Rate</span>
              <span className={classes.switch}>
                <Switch />
              </span>
            </span>
            <span className={classes.body__content}>
              <span
                style={{ fontSize: '12px' }}
                className={classes.content__name}
              >
                Is Product Quantity Mulitiply
              </span>
              <span className={classes.switch}>
                <Switch />
              </span>
            </span>
          </div>
        </div>
      </div>
    </>
  );
};
