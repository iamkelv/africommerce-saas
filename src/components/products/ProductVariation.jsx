import React, { useState } from 'react';
import classes from './ProductVariation.module.css';
import { RiArrowDownSLine } from 'react-icons/ri';
import { Divider, Switch } from '@mui/material';
import MultipleSelectCheckmarks from './reusable Ui/CheckSelect';

export const ProductVariation = () => {
  const [catOptions, setCatOptions] = useState(false);
  const [brandOptions, setBrandOptions] = useState(false);
  const [attrSwitch, setAtrrSwitch] = useState(false);
  return (
    <>
      <div className={classes.product_info}>
        <div className={classes.title}>Product Variation</div>
        <Divider />
        <div className={classes.form__group}>
          <span className={classes.form__label}>
            <label htmlFor="attribute">Color</label>
          </span>
          <span
            className={classes.form__input}
            style={{
              display: 'flex !important',
              flexDirection: 'row',
            }}
          >
            <MultipleSelectCheckmarks
              names={['Red', 'Blue', 'Green', 'Pink']}
              switch={attrSwitch}
            />{' '}
            <Switch onChange={() => setAtrrSwitch((prev) => !prev)} />
          </span>
        </div>
        <div className={classes.form__group}>
          <span className={classes.form__label}>
            <label htmlFor="attribute">Attributes</label>
          </span>
          <span
            className={classes.form__input}
            style={{
              display: 'flex !important',
              flexDirection: 'row',
            }}
          >
            <MultipleSelectCheckmarks
              names={['Size', 'Fabric', 'Sleeve', 'Wheel', 'Liter']}
              switch={true}
              defaultValue={'Select'}
            />{' '}
          </span>
        </div>
      </div>
    </>
  );
};
