import React, { useState } from 'react';
import classes from './StockVisibility.module.css';
import { RiArrowDownSLine } from 'react-icons/ri';
import { Divider, Switch } from '@mui/material';

export const StockVisibility = () => {
  const [catOptions, setCatOptions] = useState(false);
  const [brandOptions, setBrandOptions] = useState(false);
  return (
    <>
      <div className={classes.shipping}>
        <div className={classes.title}>Stock Visibility State</div>
        <Divider />
        <div className={classes.body}>
          <div className={classes.body__wrapper}>
            <span className={classes.body__content}>
              <span className={classes.content__name}>Show Stock Quantity</span>
              <span className={classes.switch}>
                <Switch />
              </span>
            </span>
            <span className={classes.body__content}>
              <span className={classes.content__name}>
                Show Stock With Text Only
              </span>
              <span className={classes.switch}>
                <Switch />
              </span>
            </span>
            <span className={classes.body__content}>
              <span className={classes.content__name}>Hide Stock</span>
              <span className={classes.switch}>
                <Switch />
              </span>
            </span>
          </div>
        </div>
      </div>
    </>
  );
};
