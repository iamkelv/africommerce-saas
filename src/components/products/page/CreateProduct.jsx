import React, { useState } from 'react';
import classes from './CreateProduct.module.css';
import { RiArrowDownSLine } from 'react-icons/ri';
import { Divider, Switch } from '@mui/material';
import { ProductInformation } from '../ProductInformation';
import { ProductImages } from '../ProductImages';
import { ProductVidoes } from '../ProductVidoes';
import { ProductVariation } from '../ProductVariation';
import { ProductPrice } from '../ProductPrice';
import { ProductDescription } from '../ProductDescription';
import { Specification } from '../Specification';
import { Seo } from '../SEO';
import { Shipping } from '../Shipping';
import { LowStock } from '../LowStock';
import { StockVisibility } from '../StockVisibility';
import { CashOnDelivery } from '../CashOnDelivery';
import { Featured } from '../Featured';
import { Today } from '@mui/icons-material';
import { TodayDeal } from '../TodaysDeal';
import { FlashDeal } from '../FlashDeal';
import { ShippingTime } from '../ShippingTime';
import { VatTax } from '../Vat_Tax';

export const CreateProduct = () => {
  const [catOptions, setCatOptions] = useState(false);
  const [brandOptions, setBrandOptions] = useState(false);
  return (
    <>
      <div
        style={{ display: 'flex', flexDirection: 'column', padding: '1rem' }}
      >
        <div>
          <h2>Add Product</h2>
        </div>
        <div>
          <div className={classes.container}>
            <div className={classes.left__info}>
              <ProductInformation />
              <ProductImages />
              <ProductVidoes />
              <ProductVariation />
              <ProductPrice />
              <ProductDescription />
              <Specification />
              <Seo />
            </div>
            <div className={classes.right__info}>
              <Shipping />
              <LowStock />
              <StockVisibility />
              <CashOnDelivery />
              <Featured />
              <TodayDeal />
              <FlashDeal />
              <ShippingTime />
              <VatTax />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
