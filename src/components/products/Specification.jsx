import React, { useState } from 'react';
import classes from './Specification.module.css';
import { Divider } from '@mui/material';

export const Specification = () => {
  const [catOptions, setCatOptions] = useState(false);
  const [brandOptions, setBrandOptions] = useState(false);
  return (
    <>
      <div className={classes.product_info}>
        <div className={classes.title}>Product Description</div>
        <Divider />
        <div className={classes.form__group}>
          <span className={classes.form__label}>
            <label htmlFor="upload">PDF Specification</label>
          </span>
          <span className={classes.form__input}>
            <input type="file" id="upload" accept=".pdf" multiple />
            <span style={{ fontSize: '14px', marginTop: '0px' }}></span>
          </span>
        </div>
      </div>
    </>
  );
};
