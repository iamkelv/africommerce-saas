import React, { useRef, useState } from 'react';
import classes from './Seo.module.css';
import { RiArrowDownSLine } from 'react-icons/ri';
import { Divider, Switch } from '@mui/material';

export const Seo = () => {
  const [catOptions, setCatOptions] = useState(false);
  const [brandOptions, setBrandOptions] = useState(false);
  const hiddenFileInput = useRef(null);

  const handleClick = (event) => {
    hiddenFileInput.current.click();
  };
  const handleChange = (event) => {
    const fileUploaded = event.target.files[0];
  };
  return (
    <div>
      <div className={classes.seo}>
        <div className={classes.title}>SEO Meta Tags</div>
        <Divider />
        <div className={classes.form__group}>
          <span className={classes.form__label}>
            <label htmlFor="productName">Meta Title</label>
          </span>
          <span className={classes.form__input}>
            <input type="text" id="productName" placeholder="Product Name" />
          </span>
        </div>
        <div className={classes.form__group}>
          <span className={classes.form__label}>
            <label htmlFor="productName">Description</label>
          </span>
          <span className={classes.form__input}>
            <textarea
              style={{
                width: '90%',
                padding: '1rem',
                borderRadius: '5px',
                border: '1px solid gray',
                outline: 'none',
              }}
              type="text"
              id="desc"
            />
          </span>
        </div>
        <div className={classes.form__group}>
          <span className={classes.form__label}>
            <label htmlFor="upload">Meta Image</label>
          </span>
          <span className={classes.form__input}>
            <input
              type="file"
              id="upload"
              accept=".jpg, .jpeg, .png"
              multiple
            />
            <span style={{ fontSize: '14px', marginTop: '0px' }}></span>
          </span>
        </div>
      </div>
    </div>
  );
};
