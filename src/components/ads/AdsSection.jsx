import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  margin: 0 auto;
  max-width: 80%;
  margin-top: 1rem;
  gap: 1rem;
  justify-content: space-between;
  transition: all 300ms ease;
`;

const AdsContents = styled.img`
  display: flex;
  max-width: 300px;
  cursor: pointer;
  margin: 0 auto;
  max-height: 200px;
  &:hover {
    opacity: 0.9;
  }
`;
const ads = [
  '../../assets/ads1.png',
  '../../assets/ads2.png',
  '../../assets/ads3.png',
];
export const AdsSection = () => {
  return (
    <Container>
      {ads.map((ads) => (
        <AdsContents src={ads} key={ads} />
      ))}
    </Container>
  );
};
